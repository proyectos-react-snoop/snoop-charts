# Snoop Charts #

Ver [DEMO](https://snoop-charts.web.app/)

### Librerías utilizadas ###

* **node-sass**: permite la compilación de archivos SASS (.scss) a CSS.
  * `npm i node-sass --save-dev`

***

* **Emotion**: es una biblioteca diseñada para escribir estilos CSS con JavaScript (CSS in JS).
  * Agrega un *hash* a cada clase creada, para que todas sean diferentes y no haya ningún tipo de colisión entre ellas.
  * Styled Components -> encapsular estilos para determinado componente.
  * `npm i @emotion/core` -> para usar sólo CSS in JS.
  * `npm i @emotion/core @emotion/styled` -> para utilizar Styled Components de Emotion.
  * Documentación oficial: [**Emotion**](https://emotion.sh/docs/introduction)

***

* **react-icons**:
  * Permite utilizar iconos de distintas fuentes como:
    * *Bootstrap Icons* - **bs**
    * *Font Awesome* - **fa**
    * *Material Design Icons* - **md**
    * *Weather Icons* - **wi**
    * Entre muchos otros repositorios de iconos.
  * Se insertan dentro del código como si fueran componentes y se importan de `'react-icons/'` seguido del prefijo establecido para cada repositorio de iconos.
    * Ejemplo > `import { AiOutlineLineChart } from 'react-icons/ai';`
  * `npm install react-icons --save`
  * Documentación oficial: https://react-icons.github.io/react-icons/

***

* **react-chartjs-2 && chart.js**:
  * ### chart.js ###
    * es una librería para gráficos.
    * está basada en HTML5 y Javascript. Para la representación de gráficos utiliza Canvas y es compatible con todos los navegadores modernos (IE11 +).
    * es responsive.
    * permite la visualización de datos en 8 tipos de gráficos diferentes.
    * Documentación oficial: https://www.chartjs.org/
  * ### chartjs-2 ###
    * es una librería que toma como base a chart.js, que incorpora componentes listos para la utilización en React. 
    * Documentación oficial: [**Github**](https://github.com/jerairrest/react-chartjs-2) | [**Demo**](https://jerairrest.github.io/react-chartjs-2/)
  * `npm i react-chartjs-2 chart.js`

