export const snoopies = {
    nacionalidades: {
        valores: [ 70, 1, 19, 1, 7 ],
        paises: [ 'Argentina', 'Paraguay', 'Chile', 'Perú', 'Venezuela' ],
    },
    sexo: {
        valores: [ 31, 67 ],
        sexo: [ 'Mujeres', 'Hombres' ],
    },
    estudios: {
        valores: [ 1, 4, 3, 3, 2, 7, 16, 25, 37 ],
        estados: [ 'Postgrado abandonado', 'Postgrado completo', 'Secundario completo', 'Terciario abandonado', 'Terciario en curso', 'Terciario completo', 'Universitario abandonado', 'Universitario en curso', 'Universitario completo' ],
    },
    antiguedad: {
        valores: [ 33, 11, 18, 15, 12, 9 ],
        antiguedad: [ '0-1 año', '1-2 años', '2-5 años', '5-10 años', '10-15 años', '15-20 años' ],
    }

}