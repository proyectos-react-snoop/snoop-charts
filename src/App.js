import React from 'react';
import { ThemeProvider } from 'emotion-theming'

import theme from './styles/theme';
import Header from './components/Header/Header';
import ChartsContainer from './components/ChartsContainer/ChartsContainer';

function App() {
    return (
		<ThemeProvider theme={ theme }>
			<div className="app">
				<Header />
				<ChartsContainer />
			</div>
		</ThemeProvider>
	);
}

export default App;
