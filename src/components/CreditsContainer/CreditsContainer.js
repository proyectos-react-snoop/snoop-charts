import React from 'react';
// import { AiOutlineGitlab } from "react-icons/ai";
import { GitlabIcon as AiOutlineGitlab } from './CreditsStyles';
import { CreditsText } from './CreditsStyles';

const CreditsContainer = () => {
    return (  
        <div className="card">
            <CreditsText>Código disponible en <a href="https://gitlab.com/proyectos-react-snoop/snoop-charts/" target="_blank" rel="noopener noreferrer"><AiOutlineGitlab /></a></CreditsText>
        </div>
    );
}
 
export default CreditsContainer;