import styled from '@emotion/styled';
import { AiOutlineGitlab } from "react-icons/ai";

export const CreditsText = styled.p`
    ${
        props => {
            const { colors } = props.theme
            return `color: ${ colors.greyMedium };`
        }
    }
    display: flex;
    align-items: center;
    font-weight: 700;

    a {
        color: inherit;
        margin-left: 15px;
    }
`;

export const GitlabIcon = styled(AiOutlineGitlab)`
    font-size: 2rem;
    transition: all .25s ease-out;

    :hover{
        ${
            props => {
                const { colors } = props.theme
                return `color: ${ colors.colorGitlab };`
            }
        }

        transform: scale(1.25);
    }
`