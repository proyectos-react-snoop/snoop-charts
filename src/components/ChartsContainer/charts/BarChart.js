import React from 'react';
import { withTheme } from 'emotion-theming';
import { Bar } from 'react-chartjs-2';

const BarChart = ({ data, theme }) => {

    const { antiguedad, valores } = data;
    const { colors } = theme;

    return (
        <div className="card">
            <Bar 
                data={{
                    labels: antiguedad, // array de datos en eje X
                    datasets: [
                        {
                            data: valores, 
                            // array de datos en eje Y
                            label: 'Antigüedad',
                            backgroundColor: colors.colorPrimary2,
                            borderColor: colors.colorPrimary,
                            borderWidth: 2,
                            hoverBackgroundColor: colors.colorPrimary1, // color de fondo de la linea
                        }
                    ], 
                }}
                options = {{
                    responsive: true,
                    legend: {
                        // display: false,
                        position: 'bottom',
                    },
                    hover: {
                        mode: 'label'
                    },
                    scales: {
                        yAxes: [{
                            display: true,
                            ticks: {
                                beginAtZero: true,
                            }
                        }]
                    },
                    // title: {
                    //     display: true,
                    //     text: 'Antigüedad de Snoopies'
                    // }
                }}
            />
        </div>
    );
}
 
export default withTheme(BarChart);