import React from 'react';
import { withTheme } from 'emotion-theming';
import { HorizontalBar } from 'react-chartjs-2';

const HorizontalBarChart = ({ data, theme }) => {

    const { valores, paises } = data;
    const { colors } = theme;

    return (
        <div className="card">
            <p>Nacionalidades</p>
            <HorizontalBar 
                data={{
                    labels: paises, // array de datos en eje X
                    datasets: [
                        {
                            data: valores, 
                            // array de datos en eje Y
                            backgroundColor: colors.colorPrimary2,
                            borderColor: colors.colorPrimary,
                            borderWidth: 2,
                            hoverBackgroundColor: colors.colorPrimary1, // color de fondo de la linea
                        }
                    ], 
                }}
                options = {{
                    responsive: true,
                    legend: {
                        display: false,
                    },
                    hover: {
                        mode: 'label'
                    },
                }}
            />
        </div>
    );
}
 
export default withTheme(HorizontalBarChart);