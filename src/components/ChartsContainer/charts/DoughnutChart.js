import React from 'react';
import { withTheme } from 'emotion-theming';
import { Doughnut } from 'react-chartjs-2';
import { FaFemale, FaMale } from 'react-icons/fa';


const DoughnutChart = ({ data, theme }) => {

    const { valores, sexo } = data;
    const { colors } = theme;

    return (
        <div className="card">
            <p><FaFemale /><FaMale /> Sexo</p>
            <Doughnut 
                data={{
                    labels: sexo, // array de datos en eje X
                    datasets: [
                        {
                            fill: false,
                            // grafico relleno
                            data: valores, 
                            // array de datos en eje Y
                            // etiqueta del grafico
                            backgroundColor: [
                                colors.colorPrimary,
                                colors.colorSecondary,
                            ],
                            lineTension: 0, // lineTension -> regula el nivel de tensión de la línea. El valor 0 devuelve líneas rectas.
                        }
                    ]
                }}
                options = {{
                    responsive: true,
                    legend: {
                        position: 'bottom',
                    },
                    hover: {
                        mode: 'label'
                    },
                }}
            />
        </div>
    );
}
 
export default withTheme(DoughnutChart);