import React from 'react';

import { ChartsC, ContainerMain, Sidebar } from './ChartsContainerStyles';
import BarChart from './charts/BarChart';
import DoughnutChart from './charts/DoughnutChart';
import { snoopies } from './../../utils/data';

import './../../styles/index.scss';
import HorizontalBarChart from './charts/HorizontalBarChart';
import CreditsContainer from '../CreditsContainer/CreditsContainer';

const ChartsContainer = () => {

    const { sexo, antiguedad, nacionalidades } = snoopies;

    return (
        <ChartsC>
            <ContainerMain>
                <BarChart 
                    data={ antiguedad }
                />
                <CreditsContainer />
            </ContainerMain>
            <Sidebar>
                <DoughnutChart 
                    data={ sexo }
                />
                <HorizontalBarChart 
                    data={ nacionalidades }
                />
            </Sidebar>
        </ChartsC>
    );
}
 
export default ChartsContainer;