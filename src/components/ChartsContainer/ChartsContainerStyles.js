import styled from '@emotion/styled';

export const ChartsC = styled.section`
    display: flex;
    flex-direction: column;
    padding: 25px 0;
    width: 100%;

    @media (min-width: 768px) {
        margin: 0 auto;
        width: 80%;
    }

    @media (min-width: 1024px) {
        flex-direction: row;
        justify-content: space-around;
        padding: 50px 7.5%;
        width: 100%;
    }

    .card {
        margin-bottom: 15px;
    }

`;

export const ContainerMain = styled.div`
    margin: 0 auto;
    width: 90%;

    @media (min-width: 768px) {
        width: 80%;
    }

    @media (min-width: 1024px) {
        margin: 0;
        width: 60%;
    }

`;

export const Sidebar = styled.aside`
    margin: 0 auto;
    width: 90%;

    @media (min-width: 768px) {
        width: 80%;
    }

    @media (min-width: 1024px) {
        margin: 0;
        width: 35%;
    }
`;