import React from 'react';
import { RiBarChartGroupedLine } from "react-icons/ri";
import { withTheme } from 'emotion-theming'

import { HeaderC } from './HeaderStyles';

const Header = () => {
    return (
        <HeaderC>
            <h1><RiBarChartGroupedLine /> Snoop Charts</h1>
        </HeaderC>
    );
}

export default withTheme(Header);