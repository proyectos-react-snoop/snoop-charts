import styled from '@emotion/styled';

export const HeaderC = styled.header`
    ${
        props => {
            const { colors } = props.theme
            return `
                background-color: ${ colors.colorPrimary };
                color: ${ colors.white };
            `
        }
    }
    padding: 1rem;
    text-align: center;
`;
