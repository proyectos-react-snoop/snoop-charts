const theme = {
    colors: {
        colorPrimary:       '#d73a49',
        colorPrimary1:      '#db4d5b',
        colorPrimary2:      '#df616d',
        colorPrimary3:      '#e78891',
        colorPrimary4:      '#efb0b6',
        colorPrimary5:      '#f7d7da',
    
        colorSecondary:     '#3f51b5',
    
        white:              '#ffffff',
        greyMedium:         '#666666',

        colorGitlab:        '#f67220',
    }
}

export default theme;